QT       += core gui widgets

CONFIG   += c++11 precompile_header

PRECOMPILED_HEADER += precompiled.h

SOURCES += \
    colorbutton.cpp \
    draw.cpp \
    scene.cpp

HEADERS += \
    colorbutton.h \
    draw.h \
    precompiled.h \
    scene.h

FORMS += draw.ui

RESOURCES += resources.qrc


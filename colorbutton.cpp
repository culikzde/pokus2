#include "precompiled.h"
#include "colorbutton.h"

#include <QPainter>

ColorButton::ColorButton (QToolBar * parent, QString p_name) :
    QToolButton (parent),
    name (p_name),
    color (QColor (p_name))
{
    parent->addWidget (this);
    setText ("Color Button");
    setIcon (QIcon (getPixmap()));
    setToolTip (name);
}

QPixmap ColorButton::getPixmap()
{
    int n = 16;
    QPixmap pixmap (n, n);
    pixmap.fill (Qt::transparent);

    QPainter painter (&pixmap);
    painter.setPen (Qt::NoPen);
    painter.setBrush (color);
    painter.drawEllipse (0, 0, n, n);
    painter.end ();

    return pixmap;
}

void ColorButton::mousePressEvent (QMouseEvent *event)
{
    if (event->button() == Qt::LeftButton)
    {
        QMimeData * data = new QMimeData;
        data->setText (name);
        data->setColorData (color);

        QDrag * drag = new QDrag (this);
        drag->setMimeData (data);
        drag->setPixmap (getPixmap());
        drag->setHotSpot(QPoint (-8, -8));
        Qt::DropAction dropAction = drag->exec (Qt::MoveAction | Qt::CopyAction | Qt::LinkAction);
    }
}
